import { InjectionToken } from '@angular/core';
import { Action, ActionReducerMap, createFeatureSelector, createReducer, on } from '@ngrx/store';
import { getAllProjects, getAllProjectsFailed, getAllProjectsSuccess } from './project.actions';
import * as _ from 'lodash'
export const initialState = {
    projects: [],
    loading: true
};
 
const _projectsReducer = createReducer(
  initialState,
  on(getAllProjects, (state, action) => ({...state, projects: [], loading: true})),
  on(getAllProjectsSuccess, (state, action) => {
    return {...state, projects: _.get(action, 'projects'), loading: false}
  }),
//   on(getAllProjectsFailed, (state) => ({...state, projects: _projectsReducer.get(action, 'payload.projects'), loading: false})),
);



export interface ProjectState {
  projects: any
}

export const PROJECT_REDUCERS = new InjectionToken<ActionReducerMap<ProjectState, Action>>('PROJECT_REDUCERS', {
  factory: () => ({
    projects: _projectsReducer
  })
});


export const GetProjectsState = createFeatureSelector<ProjectState>('projects');
