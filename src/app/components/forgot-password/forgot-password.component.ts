import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import * as _ from 'lodash'
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})


export class ForgotPasswordComponent implements OnInit {
  
    constructor(public route: Router, private service: LoginService) { }
  
    ngOnInit(): void {
    }
  
    forgotPassword = () => {
      this.route.navigate(['forgot-password'])
    }
  
  
    form: FormGroup = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      password_new: new FormControl('', Validators.required),
    });
  
    formSubmitted() {
      let formValue = _.cloneDeep(this.form.value)
  
      this.service.login(formValue).subscribe(result => {
        localStorage.setItem ('token', _.get(result, 'token'));
        this.route.navigate([''])
      }, error => {
        alert("Invalid Credentials")
      })
    }
    
    register = () => {
      this.route.navigate(['/register'])
    }
  }
  