import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { CreateNewProject } from 'src/app/store/projects/project.actions';
import { ProjectState } from 'src/app/store/projects/project.reducer';
import * as _ from 'lodash'
@Component({
  selector: 'app-addnew-project',
  templateUrl: './addnew-project.component.html',
  styleUrls: ['./addnew-project.component.scss']
})
export class AddnewProjectComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddnewProjectComponent>,
    public _rootStore: Store<ProjectState>
  ) { }


  form: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    owner: new FormControl('', Validators.required),
    cost: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
  }

  _createNewProject = (data) => {
    let formValue = _.cloneDeep(this.form.value)
    this._rootStore.dispatch(CreateNewProject({data: formValue}))
    this.dialogRef.close(true)
  }

}
