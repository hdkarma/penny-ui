import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AddnewProjectComponent } from 'src/app/popups/addnew-project/addnew-project.component';
import { getAllProjects } from 'src/app/store/projects/project.actions';
import { ProjectState } from 'src/app/store/projects/project.reducer';
import { GetProjectsStateSelector, PlanningAllProjectsSelector } from 'src/app/store/projects/project.selector';
import { Router } from '@angular/router';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private dialog: MatDialog, public _rootStore: Store<ProjectState>, private route: Router) { }

  tableDataSource$ = new Observable<any>()

  ngOnInit(): void {

    this._rootStore.dispatch(getAllProjects({}))


    this.tableDataSource$  = this._rootStore.select(PlanningAllProjectsSelector)
  }

  createNewProject = () => {
    let dialogRef = this.dialog.open(AddnewProjectComponent, {
      height: '500px',
      width: '400px',
    });
    // dialogRef.afterClosed().subscribe(result => {
    //   if(result)
    //     this._rootStore.dispatch(getAllProjects({}))
    // })
  }

  logout =  () => {
    localStorage.clear()
    this.route.navigate(['login'])
  }

  displayedColumns: string[] = [ 'name', 'cost', 'owner'];

}
