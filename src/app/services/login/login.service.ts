import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseUrl: string;

  constructor (private http: HttpClient) {
    this.baseUrl = 'http://localhost:3000';
  }
  login(data) {
    return this.http.post(this.baseUrl + '/auth', data);
  }
  signup(data) {
    return this.http.post(this.baseUrl + '/users', data);
  }
  updatePassword(data) {
    return this.http.patch(this.baseUrl + '/users', data);
  }
}
