import { Component, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import * as _ from 'lodash'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(public route: Router, private service: LoginService) { }

  ngOnInit(): void {
  }


  form: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  newSignup() {
    let formValue = _.cloneDeep(this.form.value)

    this.service.signup(formValue).subscribe(result => {
      this.route.navigate(['/login'])
    }, error => {
      alert("Duplicate email")
    })
  }
  

}
