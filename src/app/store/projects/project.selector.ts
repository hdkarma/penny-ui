import { createSelector } from '@ngrx/store';
import * as _ from 'lodash'
import { GetProjectsState, ProjectState } from './project.reducer';
export const GetProjectsStateSelector = createSelector(
    GetProjectsState,
  (state: ProjectState) => state
);


export const PlanningLoadingSelector = createSelector(GetProjectsStateSelector, state => _.get(state, 'projects.loading', true));
export const PlanningAllProjectsSelector = createSelector(GetProjectsStateSelector, state => _.get(state, 'projects', []));



