import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })
  export class ProjectService {
    constructor (private http: HttpClient) {
        this.baseUrl = 'http://localhost:3000';
    }
  
    baseUrl: string;

    getAllProjects() {
      return this.http.get(this.baseUrl + '/projects');
      // return this.http.get('https://mocki.io/v1/90171e79-6ef2-4480-b49c-6dfc7a65588b');
    }


    createNewProject(data) {
      return this.http.post(this.baseUrl + '/projects', data);
      // return this.http.get('https://mocki.io/v1/90171e79-6ef2-4480-b49c-6dfc7a65588b');
    }
  }