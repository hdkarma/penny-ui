import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewProjectComponent } from './addnew-project.component';

describe('AddnewProjectComponent', () => {
  let component: AddnewProjectComponent;
  let fixture: ComponentFixture<AddnewProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddnewProjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
