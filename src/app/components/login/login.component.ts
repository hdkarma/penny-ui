import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as _ from 'lodash'
import { LoginService } from 'src/app/services/login/login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public route: Router, private service: LoginService) { }

  ngOnInit(): void {
  }

  forgotPassword = () => {
    this.route.navigate(['forgot-password'])
  }


  form: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  formSubmitted() {
    let formValue = _.cloneDeep(this.form.value)

    this.service.login(formValue).subscribe(result => {
      localStorage.setItem ('token', _.get(result, 'token'));
      localStorage.setItem ('email', _.get(this.form, 'value.email'));
      this.route.navigate([''])
    }, error => {
      alert("Invalid Credentials")
    })
  }
  
  register = () => {
    this.route.navigate(['/register'])
  }
}
