import { createAction, props } from '@ngrx/store';

export const getAllProjects = createAction('[Projects] get all projects', props<any>());
export const getAllProjectsSuccess = createAction('[Projects] all projects success', props<any>());
export const getAllProjectsFailed = createAction('[Projects] all projects failed', props<any>());


export const CreateNewProject = createAction('[Create New Project] Create new project projects', props<any>());
export const CreateNewProjectSuccess = createAction('[Create New Project] Create new project success', props<any>());
export const CreateNewProjectFailed = createAction('[Create New Project] Create new project failed', props<any>());