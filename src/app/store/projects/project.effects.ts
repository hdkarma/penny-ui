import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { ProjectService } from 'src/services/project.service';
import { CreateNewProject, CreateNewProjectSuccess, getAllProjects, getAllProjectsFailed, getAllProjectsSuccess } from './project.actions';
 
@Injectable()
export class ProjectEffects {

    // @Effect()
    // getTodos$ = this.actions$.pipe(
    //   ofType(getAllProjects),
    //   switchMap(() =>
    //     this.projectsService.getAllProjects().pipe(
    //       map((projects) => new getAllProjectsSuccess(projects)),
    //       catchError(error => of(new getAllProjectsFailed(error)))
    //     )
    //   )
    // );


    getAllProjects$ = createEffect(() => this.actions$.pipe(
        ofType(getAllProjects, CreateNewProjectSuccess),
        mergeMap(() => this.projectsService.getAllProjects()
          .pipe(
            map(response => (getAllProjectsSuccess({projects: response}))),
            catchError(() => EMPTY)
          ))
        )
      );



    createNewProject$ = createEffect(() => this.actions$.pipe(
      ofType(CreateNewProject),
      switchMap((action) => {
        const { data  } = action;
        return this.projectsService.createNewProject(data)
        .pipe(
          map(response => (CreateNewProjectSuccess({projects: response}))),
          catchError(() => EMPTY)
        )
      }))
    );
    
 
  constructor(
    private actions$: Actions,
    private projectsService: ProjectService
  ) {}
}